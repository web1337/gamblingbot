import telebot
import sqlite3
import datetime
import random

bot = telebot.TeleBot('1720593308:AAHkBjntJDkpUjHTEVzkHBxYWULyi_ua9oM')

main_menu = telebot.types.ReplyKeyboardMarkup(True, False)
main_menu.row('/👛Мой_счёт👛')
main_menu.row('/💰Собрать_деньги💰')
main_menu.row('/📜Помощь📜')
main_menu.row('/🏪Магазин🏪', '/🕹️Игры🕹️')
main_menu.row('/😎Профиль😎')


@bot.message_handler(commands=['start'])
def start(message):
    bot.send_message(message.chat.id,
                     f'Привет {message.chat.first_name}! \nЯ твой личный азартный бот '
                     f'٩(◕‿◕｡)۶\n'
                     f'Ваша начальная сумма составляет всего лишь 1000$.... Маловато.... Нада бы увеличить!\n'
                     f'Давай сыграем? 🎰😈😈\n'
                     f'Чтобы увидеть весь список моих команд, '
                     f'тебе нужно просто написать /help или нажать на кнопку "Помощь" под клавиатурой,\n'
                     f'Также ты можешь пользовать кнопками под своей клавиатурой',
                     parse_mode='html', reply_markup=main_menu)
    con = sqlite3.connect("db_users.sqlite")
    cur = con.cursor()
    result = cur.execute('SELECT *'
                         'FROM users '
                         'WHERE id_user = {}'.format(message.chat.id)).fetchall()
    now_time = datetime.datetime.now()
    if len(result) == 0:
        cur.execute("INSERT "
                    "INTO users(id_user, date_registr_user, last_coll_time) "
                    "VALUES('{}', '{}', {})".format(message.chat.id,
                                                    now_time.strftime("%Y-%m-%d"),
                                                    datetime.datetime.now().timestamp()))
        cur.execute("INSERT "
                    "INTO miners(id_user) "
                    "VALUES('{}')".format(message.chat.id))
        con.commit()


@bot.message_handler(commands=['admin.edit'])
def debug(message):
    result = int(find_db_field('users', message.chat.id, 'admin_flag', 'id_user'))
    if result == 0:
        bot.send_message(message.chat.id,
                         'У вас нет прав на использование данной командой',
                         parse_mode='html')
        return 0
    bot.send_message(message.chat.id, 'Введите команду для отладки ('
                                      'Имя таблицы, '
                                      'id пользователя (если свой id, то слово me),'
                                      'поле в базе данных и '
                                      'нужное значение. Всё записывать через символ "/")',
                     parse_mode='html')
    bot.send_message(message.chat.id, 'Текущая дата и время в формате эпохи Unix: ', parse_mode='html')
    bot.send_message(message.chat.id, datetime.datetime.now().timestamp(), parse_mode='html')

    @bot.message_handler(content_types=["text"])
    def txt(msg):
        text = msg.text.split('/')
        try:
            name_table = text[0]
            id_user = text[1]
            if id_user == 'me':
                id_user = message.chat.id
            field = text[2]
            value = text[3]
            edit_db_field(name_table, id_user, field, value)
            bot.send_message(message.chat.id,
                             'В таблице {} у пользователя с id = {} '
                             'значение поля {} изменено на {}'.format(name_table, id_user, field, value),
                             parse_mode='html')
        except Exception:
            bot.send_message(message.chat.id,
                             'Введена неправильная команда',
                             parse_mode='html')


@bot.message_handler(commands=['admin.find'])
def debug(message):
    result = int(find_db_field('users', message.chat.id, 'admin_flag', 'id_user'))
    if result == 0:
        bot.send_message(message.chat.id,
                         'У вас нет прав на использование данной командой',
                         parse_mode='html')
        return 0
    bot.send_message(message.chat.id, 'Введите команду для поиска значения поля ('
                                      'Имя таблицы, '
                                      'id пользователя (если свой id, то слово me) и '
                                      'поле в базе данных. Всё записывать через символ "/")',
                     parse_mode='html')

    @bot.message_handler(content_types=["text"])
    def txt(msg):
        text = msg.text.split('/')
        try:
            name_table = text[0]
            id_user = text[1]
            if id_user == 'me':
                id_user = message.chat.id
            field = text[2]
            res = find_db_field(name_table, id_user, field, 'id_user')
            bot.send_message(message.chat.id,
                             'В таблице {} у пользователя с id = {} '
                             'значение поля {} = {}'.format(name_table, id_user, field, res),
                             parse_mode='html')
        except Exception:
            bot.send_message(message.chat.id,
                             'Введена неправильная команда',
                             parse_mode='html')


@bot.message_handler(commands=['admin.id_users'])
def debug(message):
    con = sqlite3.connect("db_users.sqlite")
    cur = con.cursor()
    result = cur.execute('SELECT id_user '
                         'FROM users ').fetchall()
    id_users = [i[0] for i in result]
    output_line = 'Ники всех пользователей и их id:'
    for i in id_users:
        output_line += f'\n{nick_user(int(i))[0]} {nick_user(int(i))[1]}: {str(i)}'
    bot.send_message(message.chat.id,
                     output_line,
                     parse_mode='html')


@bot.message_handler(commands=['raise_money', '💰Собрать_деньги💰'])
def raise_money(message):
    old_time = float(find_db_field('users', message.chat.id, 'last_coll_time', 'id_user'))
    koef_miners_1 = find_db_field('miners', message.chat.id, 'miner_lvl_1', 'id_user') * 100
    koef_miners_2 = find_db_field('miners', message.chat.id, 'miner_lvl_2', 'id_user') * 500
    koef_miners_3 = find_db_field('miners', message.chat.id, 'miner_lvl_3', 'id_user') * 5000
    koef = koef_miners_1 + koef_miners_2 + koef_miners_3
    new_cash = ((int(datetime.datetime.now().timestamp()) - int(old_time)) // 3600) * koef
    if new_cash == 0:
        more_time = 3600 - (int(datetime.datetime.now().timestamp()) - int(old_time))
        minutes = more_time // 60
        seconds = more_time % 60
        bot.send_message(message.chat.id,
                         'Прошло слишком мало времени с последнего сбора денег\n'
                         'До следующего сбора осталось:\n'
                         '{} минут {} секунд'.format(minutes, seconds),
                         parse_mode='html')
        return 0

    change_in_amount(message.chat.id, new_cash)
    bot.send_message(message.chat.id,
                     'Вы успешно собрали деньги.\n'
                     'Собрано  {}💰\n'
                     'Теперь у вас на счету  {}💰'.format(new_cash, find_out_the_bill(message.chat.id)),
                     parse_mode='html')
    edit_db_field('users', message.chat.id, 'last_coll_time', datetime.datetime.now().timestamp())


@bot.message_handler(commands=['miners_shop'])
def menu_miner(message1):
    miner_types = telebot.types.ReplyKeyboardMarkup(True, True)
    miner_types.row('🥉Шахтёр I уровня🥉')
    miner_types.row('🥈Шахтёр II уровня🥈')
    miner_types.row('🥇Шахтёр III уровня🥇')
    miner_types.row('🚫Я уже не хочу шахтёра🚫')
    msg = bot.send_message(message1.chat.id,
                           'Какого шахтера вы хотите купить?\n\n'
                           'Шахтёр I уровня. Базовый шахтёр. \n'
                           '+100💰 в час. Стоимость - 2500💰\n\n'
                           'Шахтёр II уровня. Трудолюбивый шахтёр для продвинутых. \n'
                           '+500💰 в час. Стоимость - 10000💰\n\n'
                           'Шахтёр III уровня. Шикарный шахтёр для денежных магнатов. \n'
                           '+5000💰 в час. Стоимость - 80000💰\n',
                           reply_markup=miner_types)

    bot.register_next_step_handler(msg, buy_miner)


def buy_miner(message):
    if message.text == '🚫Я уже не хочу шахтёра🚫':
        bot.send_message(message.chat.id, 'Возвращаю на главную', reply_markup=main_menu)
        return 0
    elif message.text == '🥉Шахтёр I уровня🥉':
        if find_out_the_bill(message.chat.id) < 2500:
            not_enough_money(message, 2500)
            return 0
        change_in_amount(message.chat.id, -2500)
        edit_db_field('miners', message.chat.id, 'miner_lvl_1',
                      find_db_field('miners', message.chat.id, 'miner_lvl_1', 'id_user') + 1)
        bot.send_message(message.chat.id,
                         'Вы купили успешно нового шахтёра I уровня', reply_markup=main_menu)
    elif message.text == '🥈Шахтёр II уровня🥈':
        if find_out_the_bill(message.chat.id) < 10000:
            not_enough_money(message, 10000)
            return 0
        change_in_amount(message.chat.id, -10000)
        edit_db_field('miners', message.chat.id, 'miner_lvl_2',
                      find_db_field('miners', message.chat.id, 'miner_lvl_2', 'id_user') + 1)
        bot.send_message(message.chat.id,
                         'Вы купили успешно нового шахтёра II уровня', reply_markup=main_menu)
    elif message.text == '🥇Шахтёр III уровня🥇':
        if find_out_the_bill(message.chat.id) < 80000:
            not_enough_money(message, 80000)
            return 0
        change_in_amount(message.chat.id, -80000)
        edit_db_field('miners', message.chat.id, 'miner_lvl_3',
                      find_db_field('miners', message.chat.id, 'miner_lvl_3', 'id_user') + 1)
        bot.send_message(message.chat.id,
                         'Вы купили успешно нового шахтёра III уровня', reply_markup=main_menu)
    else:
        bot.send_message(message.chat.id, 'Вы неправльно ввели ответ. Возвращаю на главную',
                         parse_mode='html', reply_markup=main_menu)


@bot.message_handler(commands=['top'])
def top(message):
    try:
        num = int(message.text.split()[1])
        if num < 1:
            raise Exception
    except IndexError:
        bot.send_message(message.chat.id, 'Введите количество людей в топе.\n'
                                          '(Например, "/top 3")', parse_mode='html')
        return 0
    except Exception:
        bot.send_message(message.chat.id, 'Введено неправильное значение топа', parse_mode='html')
        return 0
    if num > 20:
        bot.send_message(message.chat.id, 'Ваше значение слишком велико, оно не должно превышать 20',
                         parse_mode='html')
        return 0
    con = sqlite3.connect("db_users.sqlite")
    cur = con.cursor()
    result = cur.execute('SELECT id_user, user_cash '
                         'FROM users '
                         'ORDER BY user_cash DESC '
                         'LIMIT {}'.format(num)).fetchall()
    if len(result) < num:
        bot.send_message(message.chat.id,
                         'В базе данных не столько много людей.\n'
                         'Выдан результат на топ {}'.format(len(result)),
                         parse_mode='html')
    output_line = 'Топ {} по деньгам на данный момент:\n\n'.format(len(result))
    for i in result:
        output_line += '{}💰 - {} {}\n'.format(i[1], nick_user(i[0])[0], nick_user(i[0])[1])
    bot.send_message(message.chat.id, output_line, parse_mode='html')


@bot.message_handler(commands=['shop', '🏪Магазин🏪'])
def help_shop(message):
    bot.send_message(message.chat.id,
                     'Вы решили что-то приобрести? Мы поможем вам с навигацией\n\n'
                     '/buy_property (cars/homes/planes) (номер имущества) позволяет купить вам '
                     'собственное ИМУЩЕСТВО!!! Все ваши купленные предметы будут отображаться в профиле\n'
                     'Чтобы посмотреть список автомобилей, воспользуйтесь командой /cars\n'
                     'Чтобы посмотреть список домов, воспользуйтесь командой /houses\n'
                     'Чтобы посмотреть список самолётов, воспользуйтесь командой /planes\n\n'
                     '/miners_shop перемещает вас в магазин ⛏шахтёров⛏, которые увеличивают постоянный доход из шахты',
                     parse_mode='html')


@bot.message_handler(commands=['help', '📜Помощь📜'])
def help_list(message):
    bot.send_message(message.chat.id,
                     'Вы находитесь на странице помощи по боту.\n'
                     'Список основных команд:\n'
                     '/money - узнать количество своих денег\n'
                     '/raise_money - собрать доход из шахты (шахта - постоянный источник денег, '
                     'который приносит валюту раз в час. Доход можно увеличить, купив шахтёров)\n'
                     '/shop - помощь по магазину (покупка шахтёров, машин)\n'
                     '/games - помощь по играм (тир, "ставки")\n'
                     '/top (кол-во первых людей топа) - показывает рейтинг игроков по игровой валюте',
                     parse_mode='html')


@bot.message_handler(commands=['games', '🕹️Игры🕹️'])
def help_games(message):
    bot.send_message(message.chat.id,
                     'Хотите немного поиграть в игры и заработать денег на этом? Мы поможем в этом\n\n'
                     '/rate (ставка) - игра на ставки. Да будет бог рандома!\n\n'
                     '/roulette выстрел - тир обыкновенный. '
                     'С каждым попаданием увеличивается коэффицент выигрыша. Но если вы вовремя не '
                     'заберёте деньги - весь выигрыш сгорит. Умейте вовремя остановиться.',
                     parse_mode='html')


def change_in_amount(id_user, count):
    con = sqlite3.connect("db_users.sqlite")
    cur = con.cursor()
    cur.execute("UPDATE  "
                "users "
                "SET "
                "user_cash = user_cash + {} "
                "WHERE id_user = {}".format(int(count), int(id_user)))

    con.commit()


def find_out_the_bill(id_user):
    con = sqlite3.connect("db_users.sqlite")
    cur = con.cursor()
    result = cur.execute('SELECT user_cash '
                         'FROM users '
                         'WHERE id_user = {}'.format(id_user)).fetchone()
    return result[0]


def number_of_shots_by_user(id_user):
    con = sqlite3.connect("db_users.sqlite")
    cur = con.cursor()
    result = cur.execute('SELECT number_shots '
                         'FROM users '
                         'WHERE id_user = {}'.format(id_user)).fetchone()
    return result[0]


def not_enough_money(message, cost):
    bot.send_message(message.chat.id,
                     'Не хватает денег\n'
                     'Накопите ещё {}💰, '
                     'чтобы купить данного шахтёра'.format(cost - find_out_the_bill(message.chat.id)),
                     reply_markup=main_menu)


def edit_db_field(table, id_user, field, value):
    con = sqlite3.connect("db_users.sqlite")
    cur = con.cursor()
    cur.execute("UPDATE "
                "{} "
                "SET "
                "{} = {} "
                "WHERE id_user = {}".format(table, field, value, id_user))
    con.commit()


def find_db_field(table, id_user, field, id_field):
    con = sqlite3.connect("db_users.sqlite")
    cur = con.cursor()
    result = cur.execute('SELECT {} '
                         'FROM {} '
                         'WHERE {} = {}'.format(field, table, id_field, id_user)).fetchone()
    return result[0]


@bot.message_handler(commands=['cars'])
def list_of_cars(message):
    output_line = make_OP_l_property('cars')
    bot.send_message(message.chat.id, output_line, parse_mode='html')
    bot.send_message(message.chat.id,
                     'Чтобы купить автомобиль, воспользуйтесь командой\n'
                     '/buy_property cars (номер машины)',
                     parse_mode='html')


@bot.message_handler(commands=['houses'])
def list_of_cars(message):
    output_line = make_OP_l_property('houses')
    bot.send_message(message.chat.id, output_line, parse_mode='html')
    bot.send_message(message.chat.id,
                     'Чтобы купить дом, воспользуйтесь командой\n'
                     '/buy_property houses (номер дома)',
                     parse_mode='html')


@bot.message_handler(commands=['planes'])
def list_of_cars(message):
    output_line = make_OP_l_property('planes')
    bot.send_message(message.chat.id, output_line, parse_mode='html')
    bot.send_message(message.chat.id,
                     'Чтобы купить самолёт, воспользуйтесь командой\n'
                     '/buy_property planes (номер самолёта)',
                     parse_mode='html')


def make_OP_l_property(prop):
    con = sqlite3.connect("db_users.sqlite")
    cur = con.cursor()
    result = cur.execute('SELECT * '
                         'FROM {} '.format(prop)).fetchall()
    output_line = ''
    for i in result:
        output_line += f'{str(i[0])} - {i[1]}.\nСтоимость - {str(i[2])}💰\n\n'
    return output_line


def nick_user(user_id):
    return bot.get_chat(user_id).first_name, bot.get_chat(user_id).last_name


def find_out_the_price_of_thing(thing, id_thing):
    con = sqlite3.connect("db_users.sqlite")
    cur = con.cursor()
    id_thing_name = str("id_") + str(thing)[:-1]
    cost_thing = cur.execute('SELECT price '
                             'FROM {} '
                             'WHERE {} = {}'.format(str(thing), str(id_thing_name), int(id_thing))).fetchone()
    return cost_thing[0]


def changing_the_number_of_shots(id_user, value):
    con = sqlite3.connect("db_users.sqlite")
    cur = con.cursor()
    cur.execute("UPDATE "
                "users "
                "SET "
                "number_shots = {} "
                "WHERE id_user = {}".format(int(value), int(id_user)))
    con.commit()


def buttons_for_rate(bill_now, id_user):
    markup = telebot.types.InlineKeyboardMarkup()
    markup.add(telebot.types.InlineKeyboardButton(text="ВА-БАНК!!!", callback_data=int(bill_now)))
    if bill_now >= 2:
        markup.add(
            telebot.types.InlineKeyboardButton(text="1/2 от всей суммы", callback_data=int(bill_now) // 2))
        if bill_now >= 4:
            markup.add(
                telebot.types.InlineKeyboardButton(text="1/4 от всей суммы",
                                                   callback_data=int(bill_now) // 4))
            if bill_now >= 10:
                markup.add(telebot.types.InlineKeyboardButton(text="1/10 от всей суммы",
                                                              callback_data=int(bill_now) // 10))
        bot.send_message(id_user, text="Сделайте следующую ставку", reply_markup=markup)


@bot.message_handler(commands=['rate'])
def rate_user(message, *count):
    try:
        if count:
            rate = count[0]
        else:
            rate = message.text.replace('/rate ', '')
        if rate[0] == '-':
            bot.send_message(message.chat.id,
                             '⚠Нельзя вводить отрицатеьное число',
                             parse_mode='html', reply_markup=main_menu)
        elif message.text == "/rate":
            bot.send_message(message.chat.id,
                             'Вы моежете поставить сумму, нажав на '
                             'кнопки или написать ставку вручную',
                             parse_mode='html', reply_markup=main_menu)
            bill_now = find_out_the_bill(message.chat.id)
            buttons_for_rate(bill_now, message.chat.id)
        elif rate.isdigit() != 1:
            bot.send_message(message.chat.id,
                             '⚠Вы ввели не целое число',
                             parse_mode='html', reply_markup=main_menu)
        elif int(rate) == 0:
            bot.send_message(message.chat.id,
                             '⚠Вы не можете поставить 0$. '
                             'Минимальная ставка - 1$', reply_markup=main_menu,
                             parse_mode='html')
        elif int(rate) > int(find_out_the_bill(message.chat.id)):
            bot.send_message(message.chat.id,
                             f'⚠Вы не можете вводить больше, '
                             f'чем собственный счёт. \n'
                             f'На вашем счету {find_out_the_bill(message.chat.id)}$',
                             parse_mode='html', reply_markup=main_menu)
        else:
            item_chances_rate = {
                -int(rate): 125,
                0: 50,
                int(rate): 100,

            }
            passed_rate = int(
                random.choices(list(item_chances_rate.keys()), weights=list(item_chances_rate.values()))[0])
            if passed_rate > 0:
                item_chances = {
                    '1': 10000,
                    '2': 1000,
                    '3': 500,
                    '5': 850,
                    '25': 40,
                    '50': 1
                }
                rate_multiplication = \
                    random.choices(list(item_chances.keys()), weights=list(item_chances.values()))[0]
                change_in_amount(message.chat.id, int(passed_rate) * int(rate_multiplication))
                bot.send_message(message.chat.id,
                                 f'Вы ВЫИГРАЛИ {passed_rate}$ x{rate_multiplication}😇😇\n'
                                 f'Теперь на вашем счету {find_out_the_bill(message.chat.id)}$',
                                 parse_mode='html', reply_markup=main_menu)
            elif passed_rate < 0:
                item_chances = {
                    '1': 100,
                    '0.75': 30,
                    '0.5': 20,
                    '0.25': 10,
                }
                rate_multiplication = \
                    random.choices(list(item_chances.keys()), weights=list(item_chances.values()))[0]
                change_in_amount(message.chat.id, int(int(passed_rate) * float(rate_multiplication)))
                bot.send_message(message.chat.id,
                                 f'Вы ПРОИГРАЛИ '
                                 f'{int(int(passed_rate) * float(rate_multiplication))}$ '
                                 f'(кэффициент {rate_multiplication} от поставленного)😐😑😒😭\n'
                                 f'Теперь на вашем счету {find_out_the_bill(message.chat.id)}$',
                                 parse_mode='html', reply_markup=main_menu)
            elif passed_rate == 0:
                bot.send_message(message.chat.id,
                                 f'Вы ничего не выиграли и не проиграли',
                                 parse_mode='html', reply_markup=main_menu)
            bill_now = find_out_the_bill(message.chat.id)
            buttons_for_rate(bill_now, message.chat.id)

    except Exception:
        print(message.text)
        bot.send_message(message.chat.id,
                         'Вы ввели команду в неправильном формате.\n'
                         'Вы моежете сделать ставку, нажав на кнопку',
                         parse_mode='html', reply_markup=main_menu)
        bill_now = find_out_the_bill(message.chat.id)
        buttons_for_rate(bill_now, message.chat.id)


@bot.message_handler(commands=['money', '👛Мой_счёт👛'])
def money(message):
    cur_cash = find_out_the_bill(message.chat.id)
    bot.send_message(message.chat.id,
                     'Количество денег на вашем счету:  {}💰'.format(cur_cash),
                     parse_mode='html')


@bot.message_handler(commands=['profile', '😎Профиль😎'])
def profile(message):
    name_user = message.chat.first_name
    last_name_user = message.chat.last_name
    cash = find_db_field('users', message.chat.id, 'user_cash', 'id_user')
    date_reg = find_db_field('users', message.chat.id, 'date_registr_user', 'id_user')
    user_id_car = int(find_db_field('users', message.chat.id, 'id_car', 'id_user'))
    name_car = find_db_field('cars', user_id_car, 'car', 'id_car')
    user_id_house = int(find_db_field('users', message.chat.id, 'id_house', 'id_user'))
    name_house = find_db_field('houses', user_id_house, 'house', 'id_house')
    user_id_plane = int(find_db_field('users', message.chat.id, 'id_plane', 'id_user'))
    name_plane = find_db_field('planes', user_id_plane, 'plane', 'id_plane')
    count_miners_1 = find_db_field('miners', message.chat.id, 'miner_lvl_1', 'id_user')
    count_miners_2 = find_db_field('miners', message.chat.id, 'miner_lvl_2', 'id_user')
    count_miners_3 = find_db_field('miners', message.chat.id, 'miner_lvl_3', 'id_user')
    output_line = f'Профиль игрока {name_user} {last_name_user}:\n\n' \
                  f'Денег на счету: {cash}💰\n' \
                  f'Дата регистрации в нашем боте: {date_reg}\n\n' \
                  f'Автомобиль: ({user_id_car}) {name_car}\n' \
                  f'Дом: ({user_id_house}) {name_house}\n'\
                  f'Самолёт: ({user_id_plane}) {name_plane}\n\n'\
                  f'Шахтёры:\n' \
                  f' 1 уровень - {count_miners_1}\n' \
                  f' 2 уровень - {count_miners_2}\n' \
                  f' 3 уровень - {count_miners_3}'
    bot.send_message(message.chat.id, output_line, parse_mode='html')


@bot.message_handler(commands=['buy_property'])
def buy_cars(message):
    try:
        thing, id_thing = message.text.split()[1], message.text.split()[2]
        if id_thing.isdigit() != 1:
            bot.send_message(message.chat.id,
                             'Вы ввели не число',
                             parse_mode='html')
        elif int(id_thing) <= 1:
            bot.send_message(message.chat.id,
                             'Число не должно быть меньше 2',
                             parse_mode='html')
        else:
            name_field = str("id_") + str(thing[:-1])
            if int(find_db_field('users', message.chat.id, name_field, 'id_user')) >= int(id_thing):
                bot.send_message(message.chat.id,
                                 'у вас уже есть такая вещь, либо лучше',
                                 parse_mode='html')
            else:
                cost_thing = find_out_the_price_of_thing(thing, id_thing)
                if cost_thing > find_out_the_bill(message.chat.id):
                    bot.send_message(message.chat.id,
                                     'Не хватает средств',
                                     parse_mode='html')
                else:
                    edit_db_field('users', message.chat.id, name_field, id_thing)
                    name = find_db_field(thing, id_thing, thing[:-1], name_field)
                    bot.send_message(message.chat.id,
                                     f'Вы приобрели {name} за {cost_thing}$',
                                     parse_mode='html')
    except IndexError:
        bot.send_message(message.chat.id,
                         'Вы не уазали тип имущества или его id',
                         parse_mode='html')
    except Exception:
        bot.send_message(message.chat.id,
                         'Вы неправильно ввели команду.\n'
                         '(Образец: "/buy_property planes x", х - id имущества)',
                         parse_mode='html')


@bot.message_handler(commands=['roulette'])
def roulette(message, *action_user_button):
    try:
        if action_user_button:
            action = action_user_button[0]
        else:
            action = message.text.split()[1]
        count_shots = int(number_of_shots_by_user(message.chat.id))
        if action == 'выстрел':
            x = random.randint(0, 1)
            if x == 1:
                changing_the_number_of_shots(message.chat.id, int(count_shots) + 1)
                bot.send_message(message.chat.id,
                                 'Вы попали в мишень🤑👍🏻🔫🔫\n'
                                 f'Ваша серия попаданий: {int(count_shots) + 1}',
                                 parse_mode='html', reply_markup=main_menu)
            else:
                changing_the_number_of_shots(message.chat.id, 0)
                bot.send_message(message.chat.id,
                                 'Вы не попали в мишень😐😢\n'
                                 'Серия попаданий сбросилась',
                                 parse_mode='html', reply_markup=main_menu)
        elif action == "забрать":
            if count_shots == 0:
                bot.send_message(message.chat.id,
                                 'У вас нет серии попаданий, поэтому вы ничего не получаете',
                                 parse_mode='html', reply_markup=main_menu)
            else:
                amount_of_money_per_shot = pow(count_shots, 2) * 100
                changing_the_number_of_shots(message.chat.id, 0)
                change_in_amount(message.chat.id, amount_of_money_per_shot)
                bot.send_message(message.chat.id,
                                 f'Вы получили {amount_of_money_per_shot} за серию попаданий {count_shots} 😃😃\n'
                                 f'На вашем счету {find_out_the_bill(message.chat.id)}$',
                                 parse_mode='html', reply_markup=main_menu)
            return 0
        markup = telebot.types.InlineKeyboardMarkup()
        markup.add(telebot.types.InlineKeyboardButton(text="выстрел", callback_data='выстрел'))
        markup.add(telebot.types.InlineKeyboardButton(text="забрать", callback_data='забрать'))
        bot.send_message(message.chat.id, text="Сделайте следующий ход", reply_markup=markup)
    except IndexError:
        bot.send_message(message.chat.id,
                         'Вы не ввели слово "выстрел" после команды',
                         parse_mode='html', reply_markup=main_menu)
    except Exception:
        bot.send_message(message.chat.id,
                         'Вы неправильно ввели команду. (Образец: "/roulette выстрел")',
                         parse_mode='html', reply_markup=main_menu)


@bot.callback_query_handler(func=lambda call: True)
def query_handler(call):
    try:
        if call.data == 'выстрел' or call.data == 'забрать':
            roulette(call.message, call.data)
        else:
            rate_user(call.message, call.data)
    except Exception:
        pass


bot.polling(none_stop=True)
